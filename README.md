# Laravel6 + Vue2 + Vue-cli (Webpack)

## Установка
- Устанавливаем пакеты composer.json:
  ``` bash
  composer install
  ```
- Устанавливаем пакеты package.json:

Выполнять в папке client
  ``` bash
  npm install
  ```
- Формируем файл .env:
  ```bash
  cp .env.example .env
  ```
    - Генерируем ключ проекта:
    ``` bash
    php artisan key:generate
    ```
## База данных
- Разворачиваем базу данных в докере (опционально):
    - Контейнер:
      ``` bash
      docker compose -f docker/database/docker-compose.yaml up -d
      ```
    - Миграции:
      ``` bash
      php artisan migrate
      ```
## Сборка фронтенда
- Dev сборка с hot reload:
``` bash
npm run serve
```
- Prod сборка:
``` bash
npm run build
```
