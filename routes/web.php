<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/users', [App\Http\Controllers\UserController::class, 'getUsers']);
Route::get('/user/{userId}', [App\Http\Controllers\UserController::class, 'getUserById']);
Route::delete('/users/{id}', [App\Http\Controllers\UserController::class, 'destroy']);
Route::put('/users/{id}', [App\Http\Controllers\UserController::class, 'updateUserActivation']);
Route::post('/users', [App\Http\Controllers\UserController::class, 'store']);
Route::put('/user/{id}', [App\Http\Controllers\UserController::class, 'update']);
Auth::routes();

Route::any('{all}', function () {
    $settings = file_get_contents('build_settings.json');
    $settingsObject = json_decode($settings);
    $content = '';
    $stream_context = stream_context_create([
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false
        ]
    ]);

    if ($settingsObject->NODE_ENV === 'development' && $settingsObject->publicPath) {
        try {
            $content = file_get_contents($settingsObject->publicPath . '/index.html', false, $stream_context);
        } catch (\Throwable $e) {
            $content = 'Фронт собран в дев режиме, однако приложение не запущено.'
                . ' Выполните "npm run serve"';
        }
    } else {
        $content = file_get_contents('index.html');
    }

    return new Response(
        $content,
        200,
        ['Content-Type' => 'text/html']
    );
})->where(['all' => '.*']);
