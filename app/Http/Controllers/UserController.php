<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUsers(Request $request): \Illuminate\Http\JsonResponse
    {
        $users = User::query();

        if ($request->has('login')) {
            $users->where('login', 'like', '%' . $request->input('login') . '%');
        }
        if ($request->has('name')) {
            $users->where('name', 'like', '%' . $request->input('name') . '%');
        }
        if ($request->has('phone')) {
            $users->where('phone', 'like', '%' . $request->input('phone') . '%');
        }

        $sort = $request->input('sort', 'id');
        $order = $request->input('order', 'asc');
        $users->orderBy($sort, $order);
        $limit = $request->input('limit', 10);
        $users = $users->paginate($limit);

        return response()->json([
            'users' => $users->items(),
            'total' => $users->total(),
        ]);
    }

    public function getUserById($id): \Illuminate\Http\JsonResponse
    {
        $user = User::find($id);

        return response()->json([
            'user' => $user,
        ]);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        User::destroy($id);

        return response()->json(['message' => 'User deleted.']);
    }

    public function updateUserActivation(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail($id);
        $active = $request->input('active', false);

        $user->setAttribute('active', $active);
        $user->save();

        return response()->json(['message' => 'признак активности пользователя успешно изменен']);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $user = User::create($request->all());

        return response()->json([
            'message' => 'User created successfully.',
            'user' => $user,
        ]);
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {

        $user = User::findOrFail($id);

        // Проверяем, имеет ли текущий пользователь право редактировать данного пользователя
        // Реализация этой проверки зависит от вашей бизнес-логики и ваших правил авторизации

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'phone' => 'nullable|string|max:20',
        ]);

        $user->name = $validatedData['name'];
        $user->phone = $validatedData['phone'];
        $user->save();

        return response()->json([
            'message' => 'Пользователь успешно обновлен.',
        ]);
    }
}
