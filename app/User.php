<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    protected $fillable = [
        'login',
        'name',
        'phone',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role');
    }
}
