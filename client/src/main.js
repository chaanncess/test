/* eslint-disable global-require */
import lodash from 'lodash'
import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueCookie from 'vue-cookie'
import VueKonva from 'vue-konva'

import router from '@/router'
import store from '@/store'
import api from './api'
import App from '@/App.vue'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/packages/theme-chalk/src/index.scss'

Vue.config.env = process.env.NODE_ENV
Vue.config.prod = process.env.NODE_ENV === 'production'

Vue.router = router
Vue.use(VueAxios, api)
Vue.use(VueKonva)
Vue.use(VueCookie)
Vue.use(ElementUI, { locale })

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x'),
    tokenStore: ['localStorage', 'cookie'],

    parseUserData: data => data
})

window._ = lodash
window.vm = new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
})

window.setCookie = VueCookie.set
