export default [
    {
        path: '/',
        name: 'home',
        component: () => import('@/views/Home.vue'),
        children: [
            {
                path: 'addUser',
                name: 'AddUserForm',
                component: () => import('@/views/AddUserForm.vue'),
                meta: {
                    title: 'добавить пользователя'
                }
            },
            {
                path: '/editUser/:id?',
                name: 'EditUserForm',
                component: () => import('@/views/EditUserForm.vue'),
                meta: {
                    title: 'редактировать пользователя'
                }
            }
        ],
        meta: {
            title: 'Test'
        }
    }

]
